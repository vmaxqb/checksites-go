package main

import (
	"crypto/tls"
	"database/sql"
	"fmt"
	iconv "github.com/djimenez/iconv-go"
	"github.com/go-ini/ini"
	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
	//"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
)

const bigSql = `
	select
		substring(group_concat(id separator ','), 1, 299) ids,
		block_str,
		min(include_time) include_time
	from
	(
	SELECT DISTINCT
		u.id,
		CONCAT('http://', u.block_str) block_str,
		d.include_time
	FROM
		decisions d
	INNER JOIN
		urls u
		ON d.id = u.decision_id AND u.type = 'domain'
	WHERE
		d.active = 1 AND
		d.blocktype = 'domain'
		AND d.db_datetime < concat( SUBDATE( CURDATE( ) , 1 ) , ' 08:00:00' )
	
	UNION
	
	SELECT DISTINCT
		u.id,
		CONCAT('https://', u.block_str) block_str,
		d.include_time
	FROM
		decisions d
	INNER JOIN
		urls u
		ON d.id = u.decision_id AND u.type = 'domain'
	WHERE
		d.active = 1 AND
		d.blocktype = 'domain'
		AND d.db_datetime < concat( SUBDATE( CURDATE( ) , 1 ) , ' 08:00:00' )
	
	UNION
	
	SELECT DISTINCT
		u.id,
		CONCAT('http://', TRIM(LEADING '.' FROM TRIM(LEADING '*' FROM u.block_str))) block_str,
		d.include_time
	FROM
		decisions d
	INNER JOIN
		urls u
		ON d.id = u.decision_id AND u.type = 'domain'
	WHERE
		d.active = 1 AND
		d.blocktype = 'domain-mask'
		AND d.db_datetime < concat( SUBDATE( CURDATE( ) , 1 ) , ' 08:00:00' )
	
	UNION
	
	SELECT DISTINCT
		u.id,
		CONCAT('https://', TRIM(LEADING '.' FROM TRIM(LEADING '*' FROM u.block_str))) block_str,
		d.include_time
	FROM
		decisions d
	INNER JOIN
		urls u
		ON d.id = u.decision_id AND u.type = 'domain'
	WHERE
		d.active = 1 AND
		d.blocktype = 'domain-mask'
		AND d.db_datetime < concat( SUBDATE( CURDATE( ) , 1 ) , ' 08:00:00' )

	UNION

	SELECT DISTINCT
		u.id,
		u.block_str,
		d.include_time
	FROM
		decisions d
	INNER JOIN
		urls u
		ON d.id = u.decision_id AND u.type = 'url'
	WHERE
		d.active = 1 AND
		d.blocktype = 'default'
		AND d.db_datetime < concat( SUBDATE( CURDATE( ) , 1 ) , ' 08:00:00' )

	) q
	group by
		block_str
	ORDER BY
		block_str
`

type bigRow struct {
	ids          string
	block_str    string
	include_time string
	retry        int
}

type retType struct {
	ids          string
	block_str    string
	include_time string
	blocked      bool
	reason       string
	title        string
	workTime     int
	checkDate    time.Time
	retry        int
}

var wg sync.WaitGroup
var config *ini.File
var debugLogger *log.Logger
var retryChan chan bigRow

var reportFilename string
var triesCount int
var threadCount int

//var debugWriter io.Writer
var debug bool

func main() {
	now := time.Now()

	config, err := ini.Load("config.ini")
	if err != nil {
		panic(err)
	}

	debug = config.Section("").Key("debug").MustBool(false)
	if debug {
		debugLogger = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	}

	threadCount = config.Section("").Key("threadCount").MustInt(5)
	reportFilename = config.Section("").Key("reportFilename").MustString("checksites_go.csv")
	triesCount = config.Section("").Key("triesCount").MustInt(2)
	sqlLimit := config.Section("").Key("sqlLimit").MustInt(0)

	bigSqlLimit := ""
	if sqlLimit > 0 {
		bigSqlLimit = fmt.Sprintf(" limit %d", sqlLimit)
	}

	resultsChan := make(chan retType)
	resultsDoneChan := make(chan int)

	go processResults(resultsChan, resultsDoneChan)

	if debug {
		debugLogger.Println("Ended starting processResults")
	}

	if debug {
		debugLogger.Println("Trying to connect to db")
	}

	db, err := sql.Open("mysql", config.Section("").Key("connection").String())
	if err != nil {
		panic(err)
	}

	if debug {
		debugLogger.Println("Setting names")
	}

	_, err = db.Exec("set names utf8")
	if err != nil {
		panic(errors.Wrap(err, "Unable to query set names utf8"))
	}

	if debug {
		debugLogger.Println("Executing bigSql")
	}

	rows, err := db.Query(bigSql + bigSqlLimit)
	if err != nil {
		panic(errors.Wrap(err, "Unable to query bigSql"))
	}

	if debug {
		debugLogger.Println("Ended executing bigSql")
	}

	var allRows []bigRow

	for rows.Next() {
		r := bigRow{}
		err := rows.Scan(&r.ids, &r.block_str, &r.include_time)
		if err != nil {
			panic(errors.Wrap(err, "Unable to scan a row of bigSql"))
		}
		r.retry = 0
		allRows = append(allRows, r)
	}
	db.Close()

	for i := 0; i < triesCount; i++ {
		allRows = startSiteChecking(allRows, resultsChan)
	}

	close(resultsChan)

	<-resultsDoneChan

	currentPath, err := os.Getwd()
	if err != nil {
		panic(errors.Wrap(err, "Cannot get current dir"))
	}

	reportDir := config.Section("").Key("reportDir").MustString(currentPath)
	reportDir += "/" + now.Format("2006-01-02")
	os.MkdirAll(reportDir, 0775)
	os.Rename(reportFilename, reportDir+"/"+reportFilename)
}

// Start an iteration of site checking
// allRows - as an array or urls with its RKN-params
// resultsChan - a channel made to write results to
func startSiteChecking(allRows []bigRow, resultsChan chan<- retType) []bigRow {
	rowsChan := make(chan bigRow)
	resultsRouterChan := make(chan retType)
	routeResultsDone := make(chan bigRow)

	go routeResults(resultsRouterChan, resultsChan, routeResultsDone)

	wg.Add(threadCount)
	for i := 0; i < threadCount; i++ {
		go checkSite(rowsChan, resultsRouterChan)
	}

	if debug {
		debugLogger.Printf("Ended starting %d checkSite threads\n", threadCount)
	}

	for _, r := range allRows {
		rowsChan <- r
	}
	close(rowsChan)

	wg.Wait()
	close(resultsRouterChan)

	var result []bigRow
	for row := range routeResultsDone {
		result = append(result, row)
	}
	return result
}

// Chooses where to send the results of checking sites
// If its reachable and we wand to recheck it (we are not out of retries yet)
// then we send it to routeResultsDone for retry
// Otherwise - send result to resultsChan i.e. report-file
func routeResults(resultsRouterChan <-chan retType, resultsChan chan<- retType, routeResultsDone chan<- bigRow) {
	var rowsForRetry []bigRow

	for res := range resultsRouterChan {
		if !res.blocked && res.retry < triesCount-1 {
			row := bigRow{
				ids:          res.ids,
				block_str:    res.block_str,
				include_time: res.include_time,
				retry:        res.retry + 1,
			}
			rowsForRetry = append(rowsForRetry, row)
		} else {
			resultsChan <- res
		}
	}

	for _, row := range rowsForRetry {
		routeResultsDone <- row
	}
	close(routeResultsDone)
}

// Checks some sites. Supposed to be a goroutine and be executed parallelly
// rowsChan - channel for urls to check
// resultsChan - a channel to write results to
func checkSite(rowsChan <-chan bigRow, resultsRouterChan chan<- retType) {
	defer wg.Done()

	bisBlockString, err := iconv.ConvertString("Башинформсвязь - информационный портал об услугах доступа в интернет, телефонной связи, телевидения",
		"utf-8", "cp1251")
	if err != nil {
		panic(errors.Wrap(err, "Unable to convert encoding"))
	}

	if debug {
		debugLogger.Println("checkSite started")
	}

	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		MaxIdleConns:    1,
		IdleConnTimeout: 1,
	}
	client := &http.Client{
		Transport: transport,
		Timeout:   30 * time.Second,
	}

	for row := range rowsChan {
		timeBegin := time.Now()

		if debug {
			debugLogger.Printf("Checking %s\n", row.block_str)
		}

		ret := retType{
			ids:          row.ids,
			block_str:    row.block_str,
			include_time: row.include_time,
			checkDate:    time.Now(),
			retry:        row.retry,
		}

		req, err := http.NewRequest(http.MethodGet, strings.TrimSpace(row.block_str), nil)
		if err != nil {
			//panic(errors.Wrap(err, fmt.Sprintf("Can't make a request data for %s", row.block_str)))
			ret.blocked = true
			ret.reason = "Incorrect URL"
		} else {
			resp, err := client.Do(req)
			if err != nil {
				ret.blocked = true
				ret.reason = err.Error()
			} else {
				var bodyString string
				bodyBytes, err := ioutil.ReadAll(resp.Body)
				if err == nil {
					bodyString = string(bodyBytes)
				}
				efUrl := resp.Request.URL.String()

				if resp.StatusCode != 200 {
					ret.blocked = true
					ret.reason = fmt.Sprintf("Server error (code = %s)", resp.Status)
				} else if efUrl == "http://eais.rkn.gov.ru/" ||
					efUrl == "http://notify.bashtel.ru/block-rkn/" ||
					strings.Index(efUrl, "http://warning.rt.ru/") != -1 {
					ret.blocked = true
					ret.reason = fmt.Sprintf("Redirect to %s", efUrl)
				} else if strings.Index(bodyString, "ФЕДЕРАЛЬНАЯ СЛУЖБА ПО НАДЗОРУ В СФЕРЕ СВЯЗИ, ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ И МАССОВЫХ КОММУНИКАЦИЙ") != -1 ||
					strings.Index(bodyString, bisBlockString) != -1 {
					ret.blocked = true
					ret.reason = "RKN blockpage"
				} else {
					var title string
					titleAr := regexp.MustCompile(`(?s)<title>(.*?)<\/title>`).FindStringSubmatch(bodyString)
					if titleAr != nil {
						title = regexp.MustCompile(`\s+`).ReplaceAllString(strings.TrimSpace(titleAr[1]), " ")
						title = strings.Replace(title, ";", ",", -1)
					}
					ret.blocked = false
					ret.title = title
					if efUrl != row.block_str {
						ret.reason = fmt.Sprintf("Redirect to %s", efUrl)
					}
				}

				resp.Body.Close()
			}
		}

		// not in 1.11
		//client.CloseIdleConnections()

		ret.workTime = int(time.Now().Sub(timeBegin).Seconds())
		resultsRouterChan <- ret

		if debug {
			debugLogger.Printf("Done checking %s\n", row.block_str)
		}
	}
}

// Writes results of checking to the report-file
// resultsChan - reads results of checking from this channel
// resultsDoneChan - writes 1 there when the function is done
func processResults(resultsChan <-chan retType, resultsDoneChan chan<- int) {
	if debug {
		debugLogger.Println("Inside processResults: started\n")
	}
	f, err := os.OpenFile(reportFilename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(errors.Wrap(err, "Unable to open report file"))
	}
	defer f.Close()

	if debug {
		debugLogger.Println("Inside processResults: csv-file openned and defer-closed")
	}

	for res := range resultsChan {
		blockedStr := "Blocked"
		if !res.blocked {
			blockedStr = "REACHABLE"
		}
		protocol := "http"
		if strings.Index(res.block_str, "https") == 0 {
			protocol = "https"
		}

		f.WriteString(fmt.Sprintf(
			"%s;%s;%s;%s;%s;%s;%s;%d;%s\n",
			res.ids,
			res.checkDate.Format("2006-01-02 03:04:05"),
			blockedStr,
			res.title,
			res.reason,
			res.include_time,
			protocol,
			res.workTime,
			res.block_str,
		))
	}

	resultsDoneChan <- 1
}
